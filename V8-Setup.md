
## V8 Setup

### Prerequisites

- Install Xcode (Avaliable on the Mac App Store)
- Install Xcode Command Line Tools (Preferences > Downloads)
- Install depot_tools
    - `git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git`
    - `sudo nano ~/.bash_profile`
    - Add `export PATH=/path/to/depot_tools:"$PATH"` (it's important that depot_tools comes first here)
    - `source ~/.bash_profile`
    - From the directory you want to install V8 into, run `gclient`

### Build V8

- `fetch v8`
- `cd v8`
- `gclient sync`
- `tools/dev/v8gen.py x64.debug`
- `ninja -C out.gn/x64.debug`

recommend adding these to your `.bash_profile`: (.zshrc)
- `sudo nano ~/.bash_profile`   (`sudo nano ~/.zshrc`)
- Add `alias d8=/path/to/v8/repo/out.gn/x64.optdebug/d8`
- Add `alias tick-processor=/path/to/v8/repo/tools/mac-tick-processor`
- Add `export D8_PATH="/path/to/v8/repo/out.gn/x64.optdebug"`
- `source ~/.bash_profile` (`source ~/.zshrc`)
