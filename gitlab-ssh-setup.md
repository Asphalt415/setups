## GitLab SSH Setup

### Generate SSH-Key

- Go to `.ssh` Directory

```bash
cd ~/.ssh
```

- Create SSH-Key

```bash
ssh-keygen -t rsa -C "email-address"
```

### Register SSH-Key

- Start SSH-Agent

```bash
eval $(ssh-agent -s)
```

- Add Key to agent

```bash
ssh-add -K ~/.ssh/id_rsa
```

- Create Config-File

```bash
vi ~/.ssh/config
```
```
Host gitlab.com
   AddKeysToAgent yes
   UseKeychain yes   
   IdentityFile ~/.ssh/id_rsa
```

### Add SSH-Key to GitLab

Go to `User Settings` -> `SSH-Keys`
